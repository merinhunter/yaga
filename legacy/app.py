#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2017 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Manrique Lopez <jsmanrique@bitergia.com>

import os

from flask import Flask, request, redirect, url_for, render_template

import pandas as pd

import backend

#data_csv = pd.read_csv('data.csv')

#data = data_csv.reset_index().to_json(orient='records')
#data = data_csv.reset_index().to_dict(orient='records')
#print(data)

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
@app.route('/home', methods=['GET', 'POST'])
def index(err=None):
    """
    Render index page
    """
    if request.method == 'POST':
        ecosystem = request.form['name']
        app.logger.info('Requested data for: %s', ecosystem)
        os.makedirs('static/data/'+ecosystem, exist_ok=True)
        repos = backend.github_git_repositories(ecosystem)        
        app.logger.info('Repositories: %s', repos)

        git_data = backend.get_data(repos)
        git_data.to_csv('static/data/'+ ecosystem + '/data.csv', index=False)        
    
    ecosystems = next(os.walk('./static/data/'))[1]

    app.logger.info('Ecosystems: %s', ecosystems)

    return render_template('index.html', ecosystems=ecosystems, err=err)

@app.route('/<ecosystem>')
def ecosystem(ecosystem, err=None):    
    """
    Render ecosystem page
    """
    try:
        data = pd.read_csv('static/data/' + ecosystem + '/data.csv')
        app.logger.info('Loaded %s:', data.info())
    except:
        app.logger.info('Error getting %s data', ecosystem)
        data = pd.DataFrame() #Empty data frame
    return render_template('ecosystem.html', ecosystem=ecosystem, data=data.reset_index().to_dict(orient='records'), err=err)

@app.route('/<ecosystem>/profiles', strict_slashes=False)
def profiles(ecosystem, err=None):
    """
    Render profiles page
    """
    try:
        profiles = pd.read_csv('data/' + ecosystem + '/profiles.csv')
    except:
        app.logger.info('Error getting profiles data')
        data = pd.read_csv('static/data/' + ecosystem + '/data.csv')
        profiles = backend.get_authors(data)
        profiles.to_csv('static/data/' + ecosystem + '/profiles.csv', index=False)
    backend.improve_affiliation(ecosystem, profiles)
    unique_profiles = profiles.drop_duplicates(subset=['uuid'], keep='first')
    u_profiles_names = unique_profiles[['name', 'uuid']]
    return render_template('profiles.html', ecosystem=ecosystem, profiles=u_profiles_names.reset_index().to_dict(orient='records'), err=err)

@app.route('/<ecosystem>/profiles/merge', methods=['POST'])
def merge_profiles(ecosystem, err=None):
    """
    Merge a set of profiles
    """
    profile_uuids = request.form.getlist('uuid')
    app.logger.info('Merge profiles %s', profile_uuids)
    backend.merge_uuids(ecosystem, profile_uuids)
    return redirect(url_for('profiles', ecosystem=ecosystem))

@app.route('/<ecosystem>/profile/<uuid>')
def profile(ecosystem, uuid, err=None):
    """
    Render profile page
    """
    profiles = pd.read_csv('static/data/' + ecosystem + '/profiles.csv')
    ids = profiles.loc[profiles.uuid == uuid]
    name = ids['name'].unique()[0]
    return render_template('profile.html', ecosystem=ecosystem, name=str(name), identities=ids.reset_index().to_dict(orient='records'), err=err)

if __name__ == '__main__':
    app.run(debug=True, threaded = True)
