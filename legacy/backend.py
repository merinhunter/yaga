#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2017 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Manrique Lopez <jsmanrique@bitergia.com>

from threading import Thread

import time

import requests

import uuid

#from dateutil import parser

from perceval.backends.core.git import Git

#import numpy as np
import pandas as pd

from queue import Queue

import yaml

def git_repo_data(gitURI):
    """ Returns git commits data as Pandas.DataFrame
        from a given a git URI.
    """
    try:
        r = requests.head(gitURI)
    except:
        return None
    
    git_repo = gitURI.split('/')[-1]
    
    data_repository = Git(uri=gitURI, gitpath='/tmp/{}'.format(git_repo))
        
    df = pd.DataFrame()
    
    for commit in data_repository.fetch():
        df = df.append({'author': commit['data']['Author'],
                    'commit': commit['data']['commit'],
                    'repository': git_repo,
                    'date': commit['data']['AuthorDate']
                    },
                    ignore_index=True)

    return df

def github_git_repositories(name):
    """ Returns the list of git repositories URI
        from a given GitHub organization o username
    """
    query = "org:{}".format(name)
    page = 1
    repos = []
    
    r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
    items = r.json()['items']
    
    while len(items) > 0:
        time.sleep(5)
        for item in items:
            repos.append(item['clone_url'])
        page += 1
        r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
        try:
            items = r.json()['items']
        except:
            print('Error getting the repos list')
            print(r.json())
    
    return repos


def get_data(repositories):
    """ Returns git commits data as Pandas.DataFrame
        from a list of git repositories URIs
    """
    dataframes = []

    threads = []

    que = Queue()

    for repo in repositories:
        t = Thread(target=lambda q, arg1: q.put(git_repo_data(arg1)), args=(que, repo))
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

    while not que.empty():
        result = que.get()
        dataframes.append(result)
    
    git_data = pd.concat(dataframes)
    
    git_data['date'] = pd.to_datetime(git_data['date'])
    
    return git_data

def get_authors(commits_data):
    authors = pd.DataFrame()
    authors['username'] = commits_data.author.unique()
    authors['name'] = authors['username'].map(lambda uname: uname.split('<')[0][:-1])
    authors['email'] = authors['username'].map(lambda uname: uname.split('<')[1].split('>')[0])
    authors['email_domain'] = authors['username'].map(lambda uname: uname.split('@')[-1][:-1])
    
    df_temp = pd.DataFrame()
    df_temp['email'] = authors.email.unique()
    df_temp['uuid'] = df_temp['email'].apply(lambda x: uuid.uuid4().hex)
    
    return authors.merge(df_temp, how='left',on='email')

def merge_uuids(ecosystem, uuids_list):
    merged_uuid = uuids_list[0]
    
    profiles = pd.read_csv('static/data/' + ecosystem + '/profiles.csv')
    
    merged_name = max(profiles.loc[profiles.uuid.isin(uuids_list), 'name'], key=len)
    
    profiles.loc[profiles.uuid.isin(uuids_list), 'uuid'] = merged_uuid
    profiles.loc[profiles.uuid == merged_uuid, 'name'] = merged_name

    profiles.to_csv('static/data/' + ecosystem + '/profiles.csv', index=False)

def improve_affiliation(ecosystem, profiles):
    
    def __affiliate(domain):
        value = 'Unknown'
        exists = False
        for key in organizations_data.keys():
            if domain in organizations_data[key]:
                value = key
                exists = True
        
        if value == 'Unknown' and not exists:
            organizations_data['Unknown'].append(domain)
    
        return value

    try:
        del(profiles['organization'])
    except KeyError:
        pass
    
    df = pd.DataFrame()
    df['email_domain'] = profiles['email_domain'].unique()
    
    try:
        with open('static/data/organizations.yml') as orgs_file:
            organizations_data = yaml.load(orgs_file)
    #except FileNotFoundError:
    except IOError:
        print('File not found')
        organizations_data = {'Unknown':[]}
    
    df['organization'] = df['email_domain'].apply(__affiliate)

    with open('static/data/organizations.yml', 'w') as f:
        yaml.dump(organizations_data, f, default_flow_style=False, allow_unicode=True)
    
    new_profiles = profiles.merge(df, how='left',on='email_domain')
    new_profiles.to_csv('static/data/' + ecosystem + '/profiles.csv', index=False)