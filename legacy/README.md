# YAGA Legacy Code

This folder stores the old YAGA version built with Flask

![yaga screenshot](yaga-screenshot.jpg)

# How to run it

The app is built on [Flask](https://flask.palletsprojects.com/), so I recommend to use a Python virtual environment to run it. The procedure would be as follows:

```
$ python3 -m venv .
$ source bin/activate
(environment)$ pip install -r requirements.txt
(environment)$ python3 app.py
```

If everything has gone right, you should check `http://localhost:5000` in your browser to start using the app

# How it works

YAGA gets information from a set of git repositories (let's call it `ecosystem`), and stores the data under `/static/data/{{ecosystem}}/`, where `{{ecosystem}}` stands for the name given to the set of git repositories.

By now, the only way to define an ecosystem is by the name of a GitHub organization or user. YAGA will only get information from non-fork repositories under such organization. (Issue).

Under `/static/data/{{ecosystem}}` you will find 2 `.csv` files:
* `data.csv` that contains commits data: date, author (name + email), commit id, repository
* `profiles.csv` that contains contributors data: author (name + email), name, email, email domain, organization, uuid

There is an special file under `/static/data` called `organizations.yml`. It's a YAML file that represents organizations and the domains used for each of them. This file is used to affiliate each author (where `author` stands for the `name <email>` data) to the right organization.
