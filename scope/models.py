from django.db import models
from django.utils import timezone

# Create your models here.

class Project(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name

class GitRepo(models.Model):
    url = models.URLField()
    project = models.ForeignKey('scope.Project', on_delete=models.CASCADE)

    def __str__(self):
        return self.url

class GitHubOrg(models.Model):
    name = models.CharField(max_length=255)
    project = models.ForeignKey('scope.Project', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
