#!/usr/bin/env python

import os
import django

import time
import requests

import logging

from django.utils import timezone

from perceval.backends.core.git import Git

from dateutil.parser import parse

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'yaga.settings')
django.setup()

from scope.models import Project as ProjectScope, GitRepo, GitHubOrg
from data.models import Project, Repository, Item

#logging.basicConfig(level=logging.INFO)


def main():    

    for project in ProjectScope.objects.all():
        data_project, created = Project.objects.get_or_create(name=project.name)
    
    for github_org in GitHubOrg.objects.all():
        repos = get_github_repos(github_org.name)
        
        for repo_url in repos:
            git_repo, created = GitRepo.objects.get_or_create(url=repo_url, project=github_org.project)
            if created:
                logging.info("Added {} git repo to {} project".format(repo_url, github_org.project.name))
    
    for repo in GitRepo.objects.all():
        update_git_data_repo(repo.url, repo.project.name)

def update_git_data_repo(repo_url, project_name):
    repo_data, created = Repository.objects.get_or_create(url=repo_url, datasource='git')
    
    if created:
        logging.info('Creating {} repo'.format(repo_url))
    
    project = Project.objects.get(name=project_name)
    repo_data.projects.add(project)

    logging.info("Getting {} data".format(repo_data.url))
    get_git_data(repo_data)

    logging.info("Updating last data update for {} to: {}".format(repo_data.url, repo_data.last_update))
    repo_data.last_update = timezone.now()
    repo_data.save()
    
    

def get_git_data(repo):

    git_repo_name = repo.url.split('/')[-1]

    local_data_repository = Git(uri=repo.url, gitpath='/tmp/{}'.format(git_repo_name))

    items = []

    for commit in local_data_repository.fetch(from_date=repo.last_update):
        item = Item(
            item_id = commit['data']['commit'],
            author_name =  commit['data']['Author'],
            timestamp = parse(commit['data']['AuthorDate']),
            category = 'commit',
            repository = repo
        )
        items.append(item)

    Item.objects.bulk_create(items)
    
def get_github_repos(name):
    logging.info("Getting {} GitHub organization git repos urls".format(name))
    query = "org:{}".format(name)
    page = 1
    repos = []

    r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
    items = r.json()['items']

    while len(items) > 0:
        time.sleep(5)
        for item in items:
            repos.append(item['clone_url'])
        page += 1
        r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
        try:
            items = r.json()['items']
        except:
            print('Error getting repos list')
            print(r.json())
    
    logging.info("Search process completed: {} repos found".format(len(repos)))
    return repos

if __name__ == '__main__':
    main()