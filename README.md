# YAGA (Yet Another Git Analyzer)

For anyone interested in building their own software development analytics tool, this is a pet project to showcase
how easy it is to start, and how hard it could be to produce valuable information without projects knowledge.
(**Tip**: Hire [Bitergia](https://bitergia.com) to save time (and $$$))

YAGA (Yet Another {Git/GitHub/GitLab} Analyzer) is a pet project to play with 
[Django](https://www.djangoproject.com/), [GrimoireLab/Perceval](https://github.com/chaoss/grimoirelab-perceval), 
[Graphene](https://graphene-python.org/), and [Bokeh](https://docs.bokeh.org/en/latest/index.html) to produce some visualizations.

The aim is to build a simple tool that shall allow:
* To set up an analytics scope based on a list of repositories, GitHub organizations, or GitLab groups
* To gather data from those repositories 
* To produce some basic charts based on, at least, [CHAOSS (Community Health Analytics for Open Source Software) Metrics](https://chaoss.community/metrics)
* To curate people information to avoid duplicate contributors or to add basic affiliation information
* To have a [GraphQL endpoint](https://graphql.org/) to query generated data

**Disclaimer**: This project is not intented to be ran in production, and I recommend to go for the 
whole [GrimoireLab toolkit](https://chaoss.github.io/grimoirelab) or the [Bitergia Analytics Platform](https://bitergia.com/bitergia-analytics)
if you want a more tested, stable, and trusted solution. In any case, **you are more than welcome to contribute!**

# How to run it

The app is built on [Django](https://www.djangoproject.com/), so I recommend to use a Python virtual environment to run it. The procedure would be as follows:

```
$ python3 -m venv .
$ source bin/activate
(yaga)$ pip install -r requirements.txt
(yaga)$ python manage.py makemigrations scope
(yaga)$ python manage.py makemigrations data
(yaga)$ python manage.py makemigrations
(yaga)$ python manage.py migrate
(yaga)$ python manage.py runserver
```

If everything has gone right, you should check `http://localhost:8000` in your browser to start using the app.

# How it works

By now, to use it, you need to set up [Django superuser](https://docs.djangoproject.com/en/3.0/intro/tutorial02/#creating-an-admin-user):

```
(yaga)$ python manage.py createsuperuser
```

Enter your desired username and press enter.

```
Username: admin
```

You will then be prompted for your desired email address:

```
Email address: admin@example.com
```

The final step is to enter your password. You will be asked to enter your password twice, the second time as a confirmation of the first.

```
Password: **********
Password (again): *********
Superuser created successfully.
```

Now, you are ready. Run the server again:

```
(yaga)$ python manage.py runserver
```

And login with your created *superuser* account in `http://localhost:8000/admin` to start using the app.

## Adding projects and repositories

By now, this section is using pre-defined Django admin views, but it's intented to change in the future for a dedicated web interface.

Once logged in, you'll see the Django admin view:

![Admin view](screenshots/Screenshot_2020-05-24_admin.png)

In `SCOPE/Projects` click on `+ Add` link and fill the form with your project name. For example, let's create a project named `chaoss`:

![Add project](screenshots/Screenshot_2020-05-24_add_project.png)

Now, from the previous Admin view, or from the SCOPE view, you can add git repositories (via their URL, not the `git:...` link) or GitHub organizations and assign them to a project. For example, to add `chaoss` GitHub organization to `chaoss` project:

![Add GitHub organization](screenshots/Screenshot_2020-05-24_add_git_hub_org.png)

Now you are ready to go to next step: Gathering data

## Gathering data

This process is ran through a separate script, and the plan is to schedule it automatically in the future. By now, in the same project folder activate the virtual environment and run the `etl.py` script:

```
$ source bin/activate
(yaga)$ python etl.py

```

Depending on the size of the project, it might take a while.

If you want to see some console messages, uncomment logging paramenter related line (`#logging.basicConfig(level=logging.INFO)`)before running the script.

Once it's finished you'll be able to see some data ;-)

## Data consumption

There are 2 ways to consume the data:

* Through charts and metrics in `http://localhost:8000/data`
![YAGA data charts](screenshots/Screenshot_2020-05-24_YAGA_Data.png)

* Through data GraphQL endpoint in `http://localhost:8000/graphql`
![YAGA GraphQL](screenshots/Screenshot_2020-05-24_graphql.png)

## How to manage code contributors information

`TBD`

# License

GPL v3

# Logo

Git Logo by [Jason Long](https://twitter.com/jasonlong) is licensed under the [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/). More info in [official Git logos website](https://git-scm.com/downloads/logos).