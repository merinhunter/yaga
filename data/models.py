from django.db import models
from django.utils import timezone
import pytz

# Create your models here.

class Project(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name

class Repository(models.Model):
    KINDS = (
        ('git', 'git repository'),
        ('github', 'github repository'),
        ('gitlab', 'gitlab repository'),
    )
    url = models.URLField()
    datasource = models.TextField(choices = KINDS)
    projects = models.ManyToManyField('data.Project')
    last_update = models.DateTimeField('last update date', default=timezone.datetime(1970,1,1,0,0,tzinfo=pytz.UTC))

    def __str__(self):
        return "{} repo: {}. Last data from {}".format(self.datasource, self.url, self.last_update)

class Item(models.Model):
    item_id = models.TextField()
    author_name = models.TextField()
    timestamp = models.DateTimeField('contribution date')
    category = models.TextField()
    repository = models.ForeignKey('data.Repository', on_delete=models.CASCADE)

    def __str__(self):
        return "{} {} by {} on {}".format(self.category, self.item_id, self.author_name, self.timestamp)
    