import graphene

from django.db.models import Q

from graphene_django import DjangoObjectType

from data.models import Project, Repository, Item

class ProjectType(DjangoObjectType):
    class Meta:
        model = Project

class RepositoryType(DjangoObjectType):
    class Meta:
        model = Repository

class ItemType(DjangoObjectType):
    class Meta:
        model = Item

class Query(graphene.ObjectType):
    projects = graphene.List(
        ProjectType, 
        search=graphene.String(),
        first=graphene.Int(),
        skip=graphene.Int(),
        )
    repositories = graphene.List(
        RepositoryType, 
        search=graphene.String(),
        first=graphene.Int(),
        skip=graphene.Int(),
        )
    items = graphene.List(
        ItemType, 
        search=graphene.String(), 
        first=graphene.Int(),
        skip=graphene.Int(),
        )

    def resolve_projects(self, info, search=None, first=None, skip=None, **kwargs):
        projects = Project.objects.all()

        if search:
            filter = (
                Q(name__icontains=search)
            )
            projects = projects.filter(filter)
        
        if skip:
            projects = projects[skip:]
        
        if first:
            projects = projects[:first]

        return projects

    def resolve_repositories(self, info, search=None, first=None, skip=None, **kwargs):
        repos = Repository.objects.all()

        if search:
            filter = (
                Q(url__icontains=search)|
                Q(datasource__icontains=search)
            )
            repos = repos.filter(filter)
        
        if skip:
            repos = repos[skip:]
        
        if first:
            repos = repos[:first]

        return repos
    
    def resolve_items(self, info, search=None, first=None, skip=None, **kwargs):
        items = Item.objects.all()

        if search:
            filter = (
                Q(item_id__icontains=search) |
                Q(author_name__icontains=search) |
                Q(item_id__icontains=search) |
                Q(category__icontains=search)
            )
            items = items.filter(filter)
        
        if skip:
            items = items[skip:]
        
        if first:
            items = items[:first]

        return items